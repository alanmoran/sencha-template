Ext.define('TestApp.store.CustomerStore', {
    extend: 'Ext.data.Store',
    id: 'CustomerStore',
    config: {
        model: 'TestApp.model.CustomerModel',
        sorters: 'firstName',
        grouper: function(record) {
            return record.get('firstName')[0];
        },
        data: [{
            firstName: 'Ed',
            lastName: 'Spencer'
        }, {
            firstName: 'Tommy',
            lastName: 'Maintz'
        }, {
            firstName: 'Aaron',
            lastName: 'Conran'
        }, {
            firstName: 'Jamie',
            lastName: 'Avins'
        }]
    }
});