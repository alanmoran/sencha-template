Ext.define('TestApp.controller.Login', {
    extend: 'Ext.app.Controller',
    config: {

        refs: {
            loginPanel: 'loginScreen',
            loginButton: 'loginScreen #loginButton',
            username: 'textfield[id=loginUsername]',
            password: 'passwordfield[id=loginPassword]',
            errorPanel: 'panel[id=errMessage]'
        },

        control: {
            loginButton: {
                tap: "onLoginTap"
            }
        }
    },
    onLoginTap: function() {
            var mainPanel = Ext.create('TestApp.view.Main');
            Ext.Viewport.add(mainPanel);
            mainPanel.show();

            loginPanel = this.getLoginPanel();
            setTimeout(function() {
                loginPanel.destroy();
            }, 550);
    }
});