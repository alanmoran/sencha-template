Ext.define("TestApp.view.Login", {
    extend: "Ext.Panel",
    id: 'loginPanel',
    xtype: "loginScreen",
    requires: ["Ext.form.FieldSet", "Ext.field.Text", "Ext.field.Password", "Ext.Img"],
    config: {
        scrollable: false,
        layout: {
            type: 'vbox',
            align: 'middle',
            pack: 'center'
        },
        items: [{
            xtype:'panel',
            html:'Login Here',
            cls:'loginScreen'
        },{
            xtype: "fieldset",
            id: 'loginFieldSet',
            width: '40%',
            name: "loginForm",
            items: [{
                xtype: "textfield",
                name: "username",
                id: 'loginUsername',
                placeHolder: 'Username*',
                required: true,
                autoCapitalize: false,
                autoCorrect: false
            }, {
                xtype: 'passwordfield',
                name: 'password',
                id: 'loginPassword',
                placeHolder: 'Password*',
                required: true,
                autoCapitalize: false,
                autoCorrect: false
            }]
        }, {
            xtype: 'panel',
            id: 'errMessage',
            hidden: true,
            html: ''
        }, {
            xtype: 'button',
            id: 'loginButton',
            text: '<div align=\'center\'>Login</div>',
            width: '40%'
        }]
    }
});