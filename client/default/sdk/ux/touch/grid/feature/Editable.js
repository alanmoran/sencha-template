Ext.define('Ext.ux.touch.grid.feature.Editable', {
    extend   : 'Ext.ux.touch.grid.feature.Abstract',
    requires : 'Ext.ux.touch.grid.feature.Abstract',

    config : {
        events : {
            grid : {
                // itemdoubletap : 'handleDoubleTap',
                itemtap       : 'handleTap',
                select: 'select'
            }
        },
        extraCls : 'editable',
        activeEditor : null
    },

    onFieldBlur : function (field, e) {
         this.endEdit(this.getGrid());
    },
    select: function(grid, rec, opts) {
        // console.log('select',opts);
    },
    handleTap : function(grid, index, rowEl, rec, e) {
        // console.log('rowEl',rowEl.classList);
        this.stopEditing(e, grid, index);
        var target = e.getTarget('div.x-grid-cell'),
            cellEl = Ext.get(target);

        if (!cellEl) {
            return;
        }

        var dataIndex = cellEl.getAttribute('dataindex'),
            column    = grid.getColumn(dataIndex),
            editor    = column.editor,
            value     = rec.get(dataIndex),
            htmlValue = cellEl.getHtml();

        if(!(column.cls && column.cls.indexOf('editable') != -1)) {
            return;
        }
        if (!editor) {
            return;
        }


        this.editing = true;
        cellEl.setHtml('');

        Ext.apply(editor, {
            renderTo  : cellEl,
            value     : value,
            htmlValue : htmlValue,
            record    : rec,
            name      : dataIndex,
            rowIndex  : index
        });

        editor.field = Ext.ComponentManager.create(editor);

        editor.field.on({
            scope  : this,
            blur   : 'onFieldBlur'
        });

        this.setActiveEditor(editor);

        grid.fireEvent('editstart', grid, this, editor, dataIndex, rec);
        editor.field.focus().select();
        grid.select(index,true);
    },
    stopEditing : function(e, grid, index){
        if(this.editing){
            var editor = this.getActiveEditor();
            if (editor) {
                // if (!e.getTarget('.x-field')) {
                    this.endEdit(grid);
                // }
            }
            grid.select(index,true);
        }
        this.editing = false;
    },

    handleFieldDestroy: function(cellEl, htmlValue) {
        cellEl.setHtml(htmlValue);
    },

    endEdit : function(grid) {
        if (!grid) {
            grid = this.getGrid();
        }

        var editor    = this.getActiveEditor(),
            field     = editor.field,
            component = field.getComponent(),
            value     = component.getValue(),
            isDirty   = field.isDirty(),
            renderTo  = field.getRenderTo();

        try {
            field.destroy();
        } catch(err) {}

        var oldValue = editor.value;
        if (isDirty) {
            editor.record.set(field.getName(), value);
            grid.refresh();

            grid.fireEvent('editend', grid, this, editor, value, oldValue);
        } else {
            renderTo.setHtml(editor.htmlValue);

            grid.fireEvent('editcancel', grid, this, editor, value);
        }
        this.setActiveEditor(null);
    }
});