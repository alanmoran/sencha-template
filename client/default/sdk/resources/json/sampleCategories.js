var categories =[{
    "GroupCode": "1000000",
    "Level1": "Eye & Face Protection",
    "Level2": "",
    "Level3": ""
}, {
    "GroupCode": "1000100",
    "Level1": "Eye & Face Protection",
    "Level2": "Safety Spectacles",
    "Level3": ""
}, {
    "GroupCode": "1000200",
    "Level1": "Eye & Face Protection",
    "Level2": "Face Shields",
    "Level3": ""
}, {
    "GroupCode": "1000300",
    "Level1": "Eye & Face Protection",
    "Level2": "Goggles",
    "Level3": ""

}, {
    "GroupCode": "1000400",
    "Level1": "Eye & Face Protection",
    "Level2": "Accessories & Lens Care",
    "Level3": ""

}, {
    "GroupCode": "1100000",
    "Level1": "Head Protection",
    "Level2": "",
    "Level3": ""

}, {
    "GroupCode": "1100100",
    "Level1": "Head Protection",
    "Level2": "Helmets",
    "Level3": ""

}, {
    "GroupCode": "1100101",
    "Level1": "Head Protection",
    "Level2": "Helmets",
    "Level3": "Premium"

}, {
    "GroupCode": "1100102",
    "Level1": "Head Protection",
    "Level2": "Helmets",
    "Level3": "Quality"

}, {
    "GroupCode": "1100103",
    "Level1": "Head Protection",
    "Level2": "Helmets",
    "Level3": "Standard"

}, {
    "GroupCode": "1100200",
    "Level1": "Head Protection",
    "Level2": "Bump Caps",
    "Level3": ""

}, {
    "GroupCode": "1100201",
    "Level1": "Head Protection",
    "Level2": "Bump Caps",
    "Level3": "Premium"

}, {
    "GroupCode": "1100202",
    "Level1": "Head Protection",
    "Level2": "Bump Caps",
    "Level3": "Quality",

}, {
    "GroupCode": "1100203",
    "Level1": "Head Protection",
    "Level2": "Bump Caps",
    "Level3": "Standard",

}, {
    "GroupCode": "1100300",
    "Level1": "Head Protection",
    "Level2": "Attachments",
    "Level3": "",

}, {
    "GroupCode": "1100301",
    "Level1": "Head Protection",
    "Level2": "Attachments",
    "Level3": "JSP",

}, {
    "GroupCode": "1100302",
    "Level1": "Head Protection",
    "Level2": "Attachments",
    "Level3": "Centurion",

}, {
    "GroupCode": "1100303",
    "Level1": "Head Protection",
    "Level2": "Attachments",
    "Level3": "Protector",

}, {
    "GroupCode": "1100400",
    "Level1": "Head Protection",
    "Level2": "Forestry",
    "Level3": "",

}, {
    "GroupCode": "1200000",
    "Level1": "Ear Protection",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1200100",
    "Level1": "Ear Protection",
    "Level2": "Ear Muffs",
    "Level3": "",

}, {
    "GroupCode": "1200200",
    "Level1": "Ear Protection",
    "Level2": "Ear Plugs",
    "Level3": "",

}, {
    "GroupCode": "1200300",
    "Level1": "Ear Protection",
    "Level2": "Noise Monitoring",
    "Level3": "",

}, {
    "GroupCode": "1300000",
    "Level1": "Respiratory Protection",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1300100",
    "Level1": "Respiratory Protection",
    "Level2": "Disposable",
    "Level3": "",

}, {
    "GroupCode": "1300101",
    "Level1": "Respiratory Protection",
    "Level2": "Disposable",
    "Level3": "FFP1",

}, {
    "GroupCode": "1300102",
    "Level1": "Respiratory Protection",
    "Level2": "Disposable",
    "Level3": "FFP2",

}, {
    "GroupCode": "1300103",
    "Level1": "Respiratory Protection",
    "Level2": "Disposable",
    "Level3": "FFP2",

}, {
    "GroupCode": "1300200",
    "Level1": "Respiratory Protection",
    "Level2": "Reusable",
    "Level3": "",

}, {
    "GroupCode": "1300210",
    "Level1": "Respiratory Protection",
    "Level2": "Reusable",
    "Level3": "3M",

}, {
    "GroupCode": "1300220",
    "Level1": "Respiratory Protection",
    "Level2": "Reusable",
    "Level3": "Scott",

}, {
    "GroupCode": "1300300",
    "Level1": "Respiratory Protection",
    "Level2": "Powered Respirators",
    "Level3": "",

}, {
    "GroupCode": "1300301",
    "Level1": "Respiratory Protection",
    "Level2": "Powered Respirators",
    "Level3": "3M",

}, {
    "GroupCode": "1300302",
    "Level1": "Respiratory Protection",
    "Level2": "Powered Respirators",
    "Level3": "Scott",

}, {
    "GroupCode": "1300400",
    "Level1": "Respiratory Protection",
    "Level2": "Supplied Air",
    "Level3": "",

}, {
    "GroupCode": "1400000",
    "Level1": "Hand Protection",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1400100",
    "Level1": "Hand Protection",
    "Level2": "General",
    "Level3": "",

}, {
    "GroupCode": "1400200",
    "Level1": "Hand Protection",
    "Level2": "Specialist",
    "Level3": "",

}, {
    "GroupCode": "1400300",
    "Level1": "Hand Protection",
    "Level2": "Disposable",
    "Level3": "",

}, {
    "GroupCode": "1400400",
    "Level1": "Hand Protection",
    "Level2": "Chainmail",
    "Level3": "",

}, {
    "GroupCode": "1400500",
    "Level1": "Hand Protection",
    "Level2": "Sleeves",
    "Level3": "",

}, {
    "GroupCode": "1500000",
    "Level1": "Footwear",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1500100",
    "Level1": "Footwear",
    "Level2": "Safety Footwear",
    "Level3": "",

}, {
    "GroupCode": "1500110",
    "Level1": "Footwear",
    "Level2": "Safety Footwear",
    "Level3": "Boots",

}, {
    "GroupCode": "1500120",
    "Level1": "Footwear",
    "Level2": "Safety Footwear",
    "Level3": "Shoes",

}, {
    "GroupCode": "1500130",
    "Level1": "Footwear",
    "Level2": "Safety Footwear",
    "Level3": "Wellingtons",

}, {
    "GroupCode": "1500140",
    "Level1": "Footwear",
    "Level2": "Safety Footwear",
    "Level3": "Accessories",

}, {
    "GroupCode": "1500500",
    "Level1": "Footwear",
    "Level2": "Occupational Footwear",
    "Level3": "",

}, {
    "GroupCode": "1500510",
    "Level1": "Footwear",
    "Level2": "Occupational Footwear",
    "Level3": "Shoes",

}, {
    "GroupCode": "1500520",
    "Level1": "Footwear",
    "Level2": "Occupational Footwear",
    "Level3": "Boots",

}, {
    "GroupCode": "1500530",
    "Level1": "Footwear",
    "Level2": "Occupational Footwear",
    "Level3": "Accesories",

}, {
    "GroupCode": "1600000",
    "Level1": "Special Hazard Workwear",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1600100",
    "Level1": "Special Hazard Workwear",
    "Level2": "High Visibility",
    "Level3": "",

}, {
    "GroupCode": "1600101",
    "Level1": "Special Hazard Workwear",
    "Level2": "High Visibility",
    "Level3": "Workwear",

}, {
    "GroupCode": "1600102",
    "Level1": "Special Hazard Workwear",
    "Level2": "High Visibility",
    "Level3": "Outerwear",

}, {
    "GroupCode": "1600103",
    "Level1": "Special Hazard Workwear",
    "Level2": "High Visibility",
    "Level3": "Vests",

}, {
    "GroupCode": "1600200",
    "Level1": "Special Hazard Workwear",
    "Level2": "Chemical Resistant",
    "Level3": "",

}, {
    "GroupCode": "1600300",
    "Level1": "Special Hazard Workwear",
    "Level2": "Flame Retardant",
    "Level3": "",

}, {
    "GroupCode": "1600301",
    "Level1": "Special Hazard Workwear",
    "Level2": "Flame Retardant",
    "Level3": "Workwear",

}, {
    "GroupCode": "1600302",
    "Level1": "Special Hazard Workwear",
    "Level2": "Flame Retardant",
    "Level3": "Outerwear",

}, {
    "GroupCode": "1600303",
    "Level1": "Special Hazard Workwear",
    "Level2": "Flame Retardant",
    "Level3": "High Visibility",

}, {
    "GroupCode": "1600304",
    "Level1": "Special Hazard Workwear",
    "Level2": "Flame Retardant",
    "Level3": "Underwear",

}, {
    "GroupCode": "1600400",
    "Level1": "Special Hazard Workwear",
    "Level2": "Coldstore",
    "Level3": "",

}, {
    "GroupCode": "1600401",
    "Level1": "Special Hazard Workwear",
    "Level2": "Coldstore",
    "Level3": "Outerwear",

}, {
    "GroupCode": "1600402",
    "Level1": "Special Hazard Workwear",
    "Level2": "Coldstore",
    "Level3": "Accessories",

}, {
    "GroupCode": "1600500",
    "Level1": "Special Hazard Workwear",
    "Level2": "Food Industry",
    "Level3": "",

}, {
    "GroupCode": "1600510",
    "Level1": "Special Hazard Workwear",
    "Level2": "Food Industry",
    "Level3": "Disposable Workwear",

}, {
    "GroupCode": "1600520",
    "Level1": "Special Hazard Workwear",
    "Level2": "Food Industry",
    "Level3": "Polycotton Workwear",

}, {
    "GroupCode": "1700000",
    "Level1": "Workwear",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1700100",
    "Level1": "Workwear",
    "Level2": "Corporate",
    "Level3": "",

}, {
    "GroupCode": "1700101",
    "Level1": "Workwear",
    "Level2": "Corporate",
    "Level3": "Ladies",

}, {
    "GroupCode": "1700150",
    "Level1": "Workwear",
    "Level2": "Corporate",
    "Level3": "Gents",

}, {
    "GroupCode": "1700200",
    "Level1": "Workwear",
    "Level2": "Everyday",
    "Level3": "",

}, {
    "GroupCode": "1700210",
    "Level1": "Workwear",
    "Level2": "Everyday",
    "Level3": "Tranemo",

}, {
    "GroupCode": "1700220",
    "Level1": "Workwear",
    "Level2": "Everyday",
    "Level3": "Dickies",

}, {
    "GroupCode": "1700230",
    "Level1": "Workwear",
    "Level2": "Everyday",
    "Level3": "Mascot",

}, {
    "GroupCode": "1700240",
    "Level1": "Workwear",
    "Level2": "Everyday",
    "Level3": "Bunzl Brand",

}, {
    "GroupCode": "1700400",
    "Level1": "Workwear",
    "Level2": "Short Life",
    "Level3": "",

}, {
    "GroupCode": "1700401",
    "Level1": "Workwear",
    "Level2": "Short Life",
    "Level3": "Tyvek",

}, {
    "GroupCode": "1700402",
    "Level1": "Workwear",
    "Level2": "Short Life",
    "Level3": "Kimberly Clark",

}, {
    "GroupCode": "1700403",
    "Level1": "Workwear",
    "Level2": "Short Life",
    "Level3": "3M",

}, {
    "GroupCode": "1700404",
    "Level1": "Workwear",
    "Level2": "Short Life",
    "Level3": "Bunzl Brand",

}, {
    "GroupCode": "1800000",
    "Level1": "Weather Wear",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1800100",
    "Level1": "Weather Wear",
    "Level2": "Jackets Tranemo",
    "Level3": "Tranemo",

}, {
    "GroupCode": "1800101",
    "Level1": "Weather Wear",
    "Level2": "Jackets Elka",
    "Level3": "Elka",

}, {
    "GroupCode": "1800102",
    "Level1": "Weather Wear",
    "Level2": "Jackets Sioen",
    "Level3": "Sioen",

}, {
    "GroupCode": "1800103",
    "Level1": "Weather Wear",
    "Level2": "Jackets Tiger",
    "Level3": "Bunzl Brand",

}, {
    "GroupCode": "1800104",
    "Level1": "Weather Wear",
    "Level2": "Jackets Regatta",
    "Level3": "Regatta",

}, {
    "GroupCode": "1800105",
    "Level1": "Weather Wear",
    "Level2": "Jackets Endurance",
    "Level3": "Endurance",

}, {
    "GroupCode": "1800200",
    "Level1": "Weather Wear",
    "Level2": "Rainwear Lyngsoe",
    "Level3": "Lyngsoe",

}, {
    "GroupCode": "1800201",
    "Level1": "Weather Wear",
    "Level2": "Rainwear Sioen",
    "Level3": "Sioen",

}, {
    "GroupCode": "1800202",
    "Level1": "Weather Wear",
    "Level2": "Rainwear Elka",
    "Level3": "Elka",

}, {
    "GroupCode": "1900000",
    "Level1": "Fall Arrest",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "1900100",
    "Level1": "Fall Arrest",
    "Level2": "Sala",
    "Level3": "",

}, {
    "GroupCode": "1900200",
    "Level1": "Fall Arrest",
    "Level2": "Miller",
    "Level3": "",

}, {
    "GroupCode": "2000000",
    "Level1": "Site Safety",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2000100",
    "Level1": "Site Safety",
    "Level2": "Water Safety",
    "Level3": "",

}, {
    "GroupCode": "2000200",
    "Level1": "Site Safety",
    "Level2": "First Aid Equipment",
    "Level3": "",

}, {
    "GroupCode": "2000300",
    "Level1": "Site Safety",
    "Level2": "Torches",
    "Level3": "",

}, {
    "GroupCode": "2000400",
    "Level1": "Site Safety",
    "Level2": "Traffic Management",
    "Level3": "",

}, {
    "GroupCode": "2000500",
    "Level1": "Site Safety",
    "Level2": "Signs",
    "Level3": "",

}, {
    "GroupCode": "2000600",
    "Level1": "Site Safety",
    "Level2": "Confined Space",
    "Level3": "",

}, {
    "GroupCode": "2000700",
    "Level1": "Site Safety",
    "Level2": "Maintenance",
    "Level3": "",

}, {
    "GroupCode": "2000800",
    "Level1": "Site Safety",
    "Level2": "Sharps Protection",
    "Level3": "",

}, {
    "GroupCode": "2100000",
    "Level1": "Waste Management",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2100100",
    "Level1": "Environmental Control",
    "Level2": "Waste Segregation",
    "Level3": "",

}, {
    "GroupCode": "2100200",
    "Level1": "Environmental Control",
    "Level2": "Refuse Sacks",
    "Level3": "",

}, {
    "GroupCode": "2200000",
    "Level1": "Washroom",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2200100",
    "Level1": "Washroom",
    "Level2": "Toilet Tissue",
    "Level3": "",

}, {
    "GroupCode": "2200101",
    "Level1": "Washroom",
    "Level2": "Toilet Tissue",
    "Level3": "Bulk Pack Toilet Tissue & Dispensers",

}, {
    "GroupCode": "2200102",
    "Level1": "Washroom",
    "Level2": "Toilet Tissue",
    "Level3": "Jumbo Toilet Tissue & Dispensers",

}, {
    "GroupCode": "2200103",
    "Level1": "Washroom",
    "Level2": "Toilet Tissue",
    "Level3": "Facial Tissues",

}, {
    "GroupCode": "2200104",
    "Level1": "Washroom",
    "Level2": "Toilet Tissue",
    "Level3": "Toilet Tissue Rolls & Dispensers",

}, {
    "GroupCode": "2200200",
    "Level1": "Washroom",
    "Level2": "Disposable Towels",
    "Level3": "",

}, {
    "GroupCode": "2200201",
    "Level1": "Washroom",
    "Level2": "Disposable Towels",
    "Level3": "Folder Hand Paper Towels & Dispensers",

}, {
    "GroupCode": "2200202",
    "Level1": "Washroom",
    "Level2": "Disposable Towels",
    "Level3": "Roll Paper Hand Towels & Dispensers",

}, {
    "GroupCode": "2200300",
    "Level1": "Washroom",
    "Level2": "Skincare",
    "Level3": "",

}, {
    "GroupCode": "2200301",
    "Level1": "Washroom",
    "Level2": "Skincare",
    "Level3": "Cartridge Soap Systems",

}, {
    "GroupCode": "2200302",
    "Level1": "Washroom",
    "Level2": "Skincare",
    "Level3": "Bulk Pack Soap Systems",

}, {
    "GroupCode": "2200303",
    "Level1": "Washroom",
    "Level2": "Skincare",
    "Level3": "Table/Liquid Soup",

}, {
    "GroupCode": "2200400",
    "Level1": "Washroom",
    "Level2": "Urinal Blocks & Mats",
    "Level3": "",

}, {
    "GroupCode": "2200500",
    "Level1": "Washroom",
    "Level2": "Hand Dryers",
    "Level3": "",

}, {
    "GroupCode": "2200600",
    "Level1": "Washroom",
    "Level2": "Air Freshener Systems",
    "Level3": "",

}, {
    "GroupCode": "2200700",
    "Level1": "Washroom",
    "Level2": "Air Fresheners",
    "Level3": "",

}, {
    "GroupCode": "2300000",
    "Level1": "Spill Control Solutions",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2300100",
    "Level1": "Spill Control Solutions",
    "Level2": "Asorbents",
    "Level3": "",

}, {
    "GroupCode": "2300200",
    "Level1": "Spill Control Solutions",
    "Level2": "Pads",
    "Level3": "",

}, {
    "GroupCode": "2300300",
    "Level1": "Spill Control Solutions",
    "Level2": "Kits",
    "Level3": "",

}, {
    "GroupCode": "2500000",
    "Level1": "Skincare",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2500100",
    "Level1": "Skincare",
    "Level2": "Sanitise",
    "Level3": "",

}, {
    "GroupCode": "2500200",
    "Level1": "Skincare",
    "Level2": "Industrial",
    "Level3": "",

}, {
    "GroupCode": "2500201",
    "Level1": "Skincare",
    "Level2": "Industrial",
    "Level3": "Protect",

}, {
    "GroupCode": "2500202",
    "Level1": "Skincare",
    "Level2": "Industrial",
    "Level3": "Cleanse",

}, {
    "GroupCode": "2500203",
    "Level1": "Skincare",
    "Level2": "Industrial",
    "Level3": "Restore",

}, {
    "GroupCode": "2500300",
    "Level1": "Skincare",
    "Level2": "Catering & Food Industry",
    "Level3": "",

}, {
    "GroupCode": "2500301",
    "Level1": "Skincare",
    "Level2": "Catering & Food Industry",
    "Level3": "Cleanse",

}, {
    "GroupCode": "2500302",
    "Level1": "Skincare",
    "Level2": "Catering & Food Industry",
    "Level3": "Sanitise",

}, {
    "GroupCode": "2500303",
    "Level1": "Skincare",
    "Level2": "Catering & Food Industry",
    "Level3": "Restore",

}, {
    "GroupCode": "2500400",
    "Level1": "Skincare",
    "Level2": "Healthcare",
    "Level3": "",

}, {
    "GroupCode": "2500401",
    "Level1": "Skincare",
    "Level2": "Healthcare",
    "Level3": "Cleanse",

}, {
    "GroupCode": "2500402",
    "Level1": "Skincare",
    "Level2": "Healthcare",
    "Level3": "Sanitise",

}, {
    "GroupCode": "2500403",
    "Level1": "Skincare",
    "Level2": "Healthcare",
    "Level3": "Restore",

}, {
    "GroupCode": "2500500",
    "Level1": "Skincare",
    "Level2": "Workshop",
    "Level3": "",

}, {
    "GroupCode": "2500600",
    "Level1": "Skincare",
    "Level2": "Washoom",
    "Level3": "",

}, {
    "GroupCode": "2500601",
    "Level1": "Skincare",
    "Level2": "Washoom",
    "Level3": "Cleanse",

}, {
    "GroupCode": "2500602",
    "Level1": "Skincare",
    "Level2": "Washoom",
    "Level3": "Sanitise",

}, {
    "GroupCode": "2500603",
    "Level1": "Skincare",
    "Level2": "Washoom",
    "Level3": "Restore",

}, {
    "GroupCode": "2600000",
    "Level1": "Cleaning",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2600100",
    "Level1": "Cleaning",
    "Level2": "Eco Friendly Surface Cleaners",
    "Level3": "",

}, {
    "GroupCode": "2600150",
    "Level1": "Cleaning",
    "Level2": "Surface Cleaners",
    "Level3": "",

}, {
    "GroupCode": "2600151",
    "Level1": "Cleaning",
    "Level2": "Surface Cleaners",
    "Level3": "P&G Professional",

}, {
    "GroupCode": "2600152",
    "Level1": "Cleaning",
    "Level2": "Surface Cleaners",
    "Level3": "Diversey",

}, {
    "GroupCode": "2600153",
    "Level1": "Cleaning",
    "Level2": "Surface Cleaners",
    "Level3": "Ecolab",

}, {
    "GroupCode": "2600154",
    "Level1": "Cleaning",
    "Level2": "Surface Cleaners",
    "Level3": "Evans",

}, {
    "GroupCode": "2600155",
    "Level1": "Cleaning",
    "Level2": "Surface Cleaners",
    "Level3": "Other Brands",

}, {
    "GroupCode": "2600160",
    "Level1": "Cleaning",
    "Level2": "Furniture & Metal Polishes",
    "Level3": "",

}, {
    "GroupCode": "2600161",
    "Level1": "Cleaning",
    "Level2": "Furniture & Metal Polishes",
    "Level3": "Furniture Polish",

}, {
    "GroupCode": "2600162",
    "Level1": "Cleaning",
    "Level2": "Furniture & Metal Polishes",
    "Level3": "Metal Polishes & Glass Cleaners",

}, {
    "GroupCode": "2600200",
    "Level1": "Cleaning",
    "Level2": "Bleach",
    "Level3": "",

}, {
    "GroupCode": "2600220",
    "Level1": "Cleaning",
    "Level2": "Toilet Cleaners & Descailers",
    "Level3": "",

}, {
    "GroupCode": "2600230",
    "Level1": "Cleaning",
    "Level2": "Specialised Cleaning Chemicals",
    "Level3": "",

}, {
    "GroupCode": "2600231",
    "Level1": "Cleaning",
    "Level2": "Specialised Cleaning Chemicals",
    "Level3": "Diversey",

}, {
    "GroupCode": "2600232",
    "Level1": "Cleaning",
    "Level2": "Specialised Cleaning Chemicals",
    "Level3": "Ecolab",

}, {
    "GroupCode": "2600250",
    "Level1": "Cleaning",
    "Level2": "Disinfectants",
    "Level3": "",

}, {
    "GroupCode": "2600260",
    "Level1": "Cleaning",
    "Level2": "Infection Control",
    "Level3": "",

}, {
    "GroupCode": "2600270",
    "Level1": "Cleaning",
    "Level2": "Maintenance Products",
    "Level3": "",

}, {
    "GroupCode": "2600280",
    "Level1": "Cleaning",
    "Level2": "Laundry Products",
    "Level3": "",

}, {
    "GroupCode": "2600290",
    "Level1": "Cleaning",
    "Level2": "Cloths & Pads",
    "Level3": "",

}, {
    "GroupCode": "2600300",
    "Level1": "Cleaning",
    "Level2": "Waste Disposal",
    "Level3": "",

}, {
    "GroupCode": "2600400",
    "Level1": "Cleaning",
    "Level2": "Wash Up Liquids and Detergents",
    "Level3": "",

}, {
    "GroupCode": "2600500",
    "Level1": "Cleaning",
    "Level2": "Diversey Systems",
    "Level3": "",

}, {
    "GroupCode": "2700000",
    "Level1": "Floorcare",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2700100",
    "Level1": "Floorcare",
    "Level2": "3 Step System",
    "Level3": "",

}, {
    "GroupCode": "2700101",
    "Level1": "Floorcare",
    "Level2": "3 Step System",
    "Level3": "Strip",

}, {
    "GroupCode": "2700102",
    "Level1": "Floorcare",
    "Level2": "3 Step System",
    "Level3": "Polish",

}, {
    "GroupCode": "2700103",
    "Level1": "Floorcare",
    "Level2": "3 Step System",
    "Level3": "Maintainer",

}, {
    "GroupCode": "2700110",
    "Level1": "Floorcare",
    "Level2": "Polish",
    "Level3": "",

}, {
    "GroupCode": "2700120",
    "Level1": "Floorcare",
    "Level2": "Mops",
    "Level3": "",

}, {
    "GroupCode": "2700130",
    "Level1": "Floorcare",
    "Level2": "Mop Systems",
    "Level3": "",

}, {
    "GroupCode": "2700131",
    "Level1": "Floorcare",
    "Level2": "Mop Systems",
    "Level3": "Diversey",

}, {
    "GroupCode": "2700132",
    "Level1": "Floorcare",
    "Level2": "Mop Systems",
    "Level3": "Vileda",

}, {
    "GroupCode": "2700133",
    "Level1": "Floorcare",
    "Level2": "Mop Systems",
    "Level3": "Ecolab",

}, {
    "GroupCode": "2700140",
    "Level1": "Floorcare",
    "Level2": "Brushes",
    "Level3": "",

}, {
    "GroupCode": "2700150",
    "Level1": "Floorcare",
    "Level2": "Buckets & Wringers",
    "Level3": "",

}, {
    "GroupCode": "2700160",
    "Level1": "Floorcare",
    "Level2": "Floor Pads & Machines",
    "Level3": "",

}, {
    "GroupCode": "2700170",
    "Level1": "Floorcare",
    "Level2": "Cone & Signs",
    "Level3": "",

}, {
    "GroupCode": "2700175",
    "Level1": "Floorcare",
    "Level2": "Trollies",
    "Level3": "",

}, {
    "GroupCode": "2700180",
    "Level1": "Floorcare",
    "Level2": "Mats",
    "Level3": "",

}, {
    "GroupCode": "2700190",
    "Level1": "Floorcare",
    "Level2": "Personal Protective Equipment",
    "Level3": "",

}, {
    "GroupCode": "2700195",
    "Level1": "Floorcare",
    "Level2": "Carpet Care",
    "Level3": "",

}, {
    "GroupCode": "2800000",
    "Level1": "Window Cleaning",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "2800000",
    "Level1": "Window Cleaning",
    "Level2": "Equipment",
    "Level3": "",

}, {
    "GroupCode": "2800000",
    "Level1": "Window Cleaning",
    "Level2": "Products",
    "Level3": "",

}, {
    "GroupCode": "3000000",
    "Level1": "Industrial Wiping",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "3000100",
    "Level1": "Industrial Wiping",
    "Level2": "Cloths",
    "Level3": "",

}, {
    "GroupCode": "3000200",
    "Level1": "Industrial Wiping",
    "Level2": "Rolls",
    "Level3": "",

}, {
    "GroupCode": "4000000",
    "Level1": "Capital Equipment",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "4000101",
    "Level1": "Capital Equipment",
    "Level2": "Catering Equipment",
    "Level3": "",

}, {
    "GroupCode": "4000102",
    "Level1": "Capital Equipment",
    "Level2": "Furniture",
    "Level3": "",

}, {
    "GroupCode": "4000103",
    "Level1": "Capital Equipment",
    "Level2": "Hotel Accommodation",
    "Level3": "",

}, {
    "GroupCode": "5000000",
    "Level1": "Hotelware",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5000100",
    "Level1": "Hotelware",
    "Level2": "Steelite",
    "Level3": "",

}, {
    "GroupCode": "5000200",
    "Level1": "Hotelware",
    "Level2": "Villeroy & Boch",
    "Level3": "",

}, {
    "GroupCode": "5000300",
    "Level1": "Hotelware",
    "Level2": "Churchill",
    "Level3": "",

}, {
    "GroupCode": "5000400",
    "Level1": "Hotelware",
    "Level2": "Arcoroc",
    "Level3": "",

}, {
    "GroupCode": "5000500",
    "Level1": "Hotelware",
    "Level2": "Chef & Sommelier",
    "Level3": "",

}, {
    "GroupCode": "5100000",
    "Level1": "Cutlery",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5100100",
    "Level1": "Cutlery",
    "Level2": "Accessories",
    "Level3": "",

}, {
    "GroupCode": "5100200",
    "Level1": "Cutlery",
    "Level2": "Steak Knives",
    "Level3": "",

}, {
    "GroupCode": "5100300",
    "Level1": "Cutlery",
    "Level2": "Virtu",
    "Level3": "",

}, {
    "GroupCode": "5100400",
    "Level1": "Cutlery",
    "Level2": "Jester",
    "Level3": "",

}, {
    "GroupCode": "5100500",
    "Level1": "Cutlery",
    "Level2": "Regal Ribbon",
    "Level3": "",

}, {
    "GroupCode": "5100600",
    "Level1": "Cutlery",
    "Level2": "Endurance",
    "Level3": "",

}, {
    "GroupCode": "5100700",
    "Level1": "Cutlery",
    "Level2": "Siena",
    "Level3": "",

}, {
    "GroupCode": "5100800",
    "Level1": "Cutlery",
    "Level2": "Caroline",
    "Level3": "",

}, {
    "GroupCode": "5100900",
    "Level1": "Cutlery",
    "Level2": "Baguette",
    "Level3": "",

}, {
    "GroupCode": "5101000",
    "Level1": "Cutlery",
    "Level2": "Cambridge",
    "Level3": "",

}, {
    "GroupCode": "5101100",
    "Level1": "Cutlery",
    "Level2": "Moderno",
    "Level3": "",

}, {
    "GroupCode": "5101200",
    "Level1": "Cutlery",
    "Level2": "Ventura",
    "Level3": "",

}, {
    "GroupCode": "5101300",
    "Level1": "Cutlery",
    "Level2": "Colorado",
    "Level3": "",

}, {
    "GroupCode": "5101400",
    "Level1": "Cutlery",
    "Level2": "Cuba",
    "Level3": "",

}, {
    "GroupCode": "5101500",
    "Level1": "Cutlery",
    "Level2": "Carlton",
    "Level3": "",

}, {
    "GroupCode": "5101600",
    "Level1": "Cutlery",
    "Level2": "Bead",
    "Level3": "",

}, {
    "GroupCode": "5101700",
    "Level1": "Cutlery",
    "Level2": "Jesmond",
    "Level3": "",

}, {
    "GroupCode": "5101800",
    "Level1": "Cutlery",
    "Level2": "Rattail",
    "Level3": "",

}, {
    "GroupCode": "5101900",
    "Level1": "Cutlery",
    "Level2": "Harley",
    "Level3": "",

}, {
    "GroupCode": "5102000",
    "Level1": "Cutlery",
    "Level2": "Dubarry",
    "Level3": "",

}, {
    "GroupCode": "5102100",
    "Level1": "Cutlery",
    "Level2": "Kings",
    "Level3": "",

}, {
    "GroupCode": "5102200",
    "Level1": "Cutlery",
    "Level2": "Plain",
    "Level3": "",

}, {
    "GroupCode": "5102300",
    "Level1": "Cutlery",
    "Level2": "Elevation",
    "Level3": "",

}, {
    "GroupCode": "5200000",
    "Level1": "Glassware",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5200100",
    "Level1": "Glassware",
    "Level2": "Wine Suites",
    "Level3": "",

}, {
    "GroupCode": "5200200",
    "Level1": "Glassware",
    "Level2": "Tumblers",
    "Level3": "",

}, {
    "GroupCode": "5200300",
    "Level1": "Glassware",
    "Level2": "Beer/Lager",
    "Level3": "",

}, {
    "GroupCode": "5200400",
    "Level1": "Glassware",
    "Level2": "Plastic Glasses",
    "Level3": "",

}, {
    "GroupCode": "5200500",
    "Level1": "Glassware",
    "Level2": "Sundry Glasses",
    "Level3": "",

}, {
    "GroupCode": "5200600",
    "Level1": "Glassware",
    "Level2": "Jugs",
    "Level3": "",

}, {
    "GroupCode": "5200700",
    "Level1": "Glassware",
    "Level2": "Dessert Ware",
    "Level3": "",

}, {
    "GroupCode": "5200800",
    "Level1": "Glassware",
    "Level2": "Sundries",
    "Level3": "",

}, {
    "GroupCode": "5300000",
    "Level1": "Bar Equipment",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5300100",
    "Level1": "Bar Equipment",
    "Level2": "Pourers, Optics & Measures",
    "Level3": "",

}, {
    "GroupCode": "5300200",
    "Level1": "Bar Equipment",
    "Level2": "Bottle Holders",
    "Level3": "",

}, {
    "GroupCode": "5300300",
    "Level1": "Bar Equipment",
    "Level2": "Bottle Openers",
    "Level3": "",

}, {
    "GroupCode": "5300400",
    "Level1": "Bar Equipment",
    "Level2": "Bar Essentials",
    "Level3": "",

}, {
    "GroupCode": "5300500",
    "Level1": "Bar Equipment",
    "Level2": "Cocktail & Bar Aids",
    "Level3": "",

}, {
    "GroupCode": "5300600",
    "Level1": "Bar Equipment",
    "Level2": "Drink Accessories",
    "Level3": "",

}, {
    "GroupCode": "5300700",
    "Level1": "Bar Equipment",
    "Level2": "Ice Buckets & Coolers",
    "Level3": "",

}, {
    "GroupCode": "5300800",
    "Level1": "Bar Equipment",
    "Level2": "Equipment",
    "Level3": "",

}, {
    "GroupCode": "5400000",
    "Level1": "Table Presentation",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5400100",
    "Level1": "Table Presentation",
    "Level2": "Chafing Dishes",
    "Level3": "",

}, {
    "GroupCode": "5400200",
    "Level1": "Table Presentation",
    "Level2": "Serving Displays",
    "Level3": "",

}, {
    "GroupCode": "5400300",
    "Level1": "Table Presentation",
    "Level2": "Beverage Serving",
    "Level3": "",

}, {
    "GroupCode": "5400400",
    "Level1": "Table Presentation",
    "Level2": "Cook/Serving",
    "Level3": "",

}, {
    "GroupCode": "5400500",
    "Level1": "Table Presentation",
    "Level2": "Food Service",
    "Level3": "",

}, {
    "GroupCode": "5400600",
    "Level1": "Table Presentation",
    "Level2": "Table Top",
    "Level3": "",

}, {
    "GroupCode": "5400700",
    "Level1": "Table Presentation",
    "Level2": "Trays",
    "Level3": "",

}, {
    "GroupCode": "5400800",
    "Level1": "Table Presentation",
    "Level2": "Sundries",
    "Level3": "",

}, {
    "GroupCode": "5500000",
    "Level1": "Disposables",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5500100",
    "Level1": "Disposables",
    "Level2": "Candles",
    "Level3": "",

}, {
    "GroupCode": "5500200",
    "Level1": "Disposables",
    "Level2": "Table Top",
    "Level3": "",

}, {
    "GroupCode": "5500300",
    "Level1": "Disposables",
    "Level2": "Food to Go",
    "Level3": "",

}, {
    "GroupCode": "5500400",
    "Level1": "Disposables",
    "Level2": "Food Handling",
    "Level3": "",

}, {
    "GroupCode": "5500500",
    "Level1": "Disposables",
    "Level2": "Food Handling Gloves",
    "Level3": "",

}, {
    "GroupCode": "5600000",
    "Level1": "Kitchen Utensils",
    "Level2": "",
    "Level3": "",

}, {
    "GroupCode": "5600100",
    "Level1": "Kitchen Utensils",
    "Level2": "Utensils",
    "Level3": "",

}, {
    "GroupCode": "5600200",
    "Level1": "Kitchen Utensils",
    "Level2": "Knives",
    "Level3": "",

}, {
    "GroupCode": "5600300",
    "Level1": "Kitchen Utensils",
    "Level2": "Pots & Pans",
    "Level3": "",

}, {
    "GroupCode": "5600400",
    "Level1": "Kitchen Utensils",
    "Level2": "Bakeware",
    "Level3": "",

}, {
    "GroupCode": "5600500",
    "Level1": "Kitchen Utensils",
    "Level2": "Food Storage & Labels",
    "Level3": "",

}, {
    "GroupCode": "5600600",
    "Level1": "Kitchen Utensils",
    "Level2": "Signs & First Aid",
    "Level3": "",

}, {
    "GroupCode": "5600700",
    "Level1": "Kitchen Utensils",
    "Level2": "Wipers",
    "Level3": "",

}];