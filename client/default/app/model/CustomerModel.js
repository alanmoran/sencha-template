Ext.define('TestApp.model.CustomerModel', {
  extend: 'Ext.data.Model',
  id: 'CustomerModel',
  config: {
    idProperty: 'firstName',
    fields: [{
      name: 'firstName',
      type: 'string'
    }, {
      name: 'lastName',
      type: 'string'
    }]
  }
});