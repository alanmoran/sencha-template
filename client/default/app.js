Ext.Loader.setPath({
    'Ext': 'sdk',
    'Ux' : 'ux'
});

Ext.Loader.setConfig({disableCaching : false});

Ext.Ajax.setConfig({enabled: true, disableCaching : false});

Ext.application({
    name: 'TestApp',

    requires: [],
    models: [
    'CustomerModel',
    'TreeStore'
    ],
    stores: [
    'CustomerStore',
    'TreeStore'
    ],
    views: [
        'Main',
        'Login',
        'List1',
        'NavScreen',
        'TreeList'
    ],
    controllers: [
        'Login',
        'NavScreen'
    ],
    launch: function() {
        
        Ext.Viewport.add({
            xclass: 'TestApp.view.Login'
        });
    }
});